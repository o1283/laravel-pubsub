<?php

namespace L2T\PubSub;

use Illuminate\Support\Str;
use Illuminate\Contracts\Queue\Queue;
use Google\Cloud\PubSub\PubSubClient;
use Illuminate\Queue\Connectors\ConnectorInterface;

class Connector implements ConnectorInterface
{
    /**
     * Default queue name.
     *
     * @var string
     */
    protected string $default_queue = 'default';

    /**
     * Establish a queue connection.
     *
     * @param array $config
     * @return QueueProcessor|Queue
     */
    public function connect(array $config): QueueProcessor|Queue
    {
        $gcp_config = $this->transformConfig($config);

        return new QueueProcessor(
            new PubSubClient($gcp_config),
            $config['queue'] ?? $this->default_queue,
            $config['create_topics'] ?? true,
            $config['create_subscriptions'] ?? true,
            $config['queue_prefix'] ?? ''
        );
    }

    /**
     * Transform the config to key => value array.
     *
     * @param array $config
     *
     * @return array
     */
    protected function transformConfig(array $config): array
    {
        return array_reduce(array_map([$this, 'transformConfigKeys'], $config, array_keys($config)), function ($carry, $item) {
            $carry[$item[0]] = $item[1];
            return $carry;
        }, []);
    }

    /**
     * Transform the keys of config to camelCase.
     *
     * @param string $item
     * @param string $key
     *
     * @return array
     */
    protected function transformConfigKeys(string $item, string $key): array
    {
        return [Str::camel($key), $item];
    }
}
