<?php

namespace L2T\PubSub;

use Illuminate\Support\Str;
use Illuminate\Queue\Queue;
use Google\Cloud\PubSub\Topic;
use Google\Cloud\PubSub\Message;
use Illuminate\Contracts\Queue\Job;
use Google\Cloud\PubSub\Subscription;
use Google\Cloud\PubSub\PubSubClient;
use Illuminate\Contracts\Queue\Queue as QueueContract;

class QueueProcessor extends Queue implements QueueContract
{
    /**
     * Default queue name.
     *
     * @var string
     */
    protected string $default;

    /**
     * Default subscriber.
     *
     * @var string
     */
    protected mixed $subscriber;

    /**
     * The PubSubClient instance.
     *
     * @var PubSubClient
     */
    protected PubSubClient $pubsub;

    /**
     * Create topics automatically.
     *
     * @var bool
     */
    protected mixed $topicAutoCreation;

    /**
     * Create subscriptions automatically.
     *
     * @var bool
     */
    protected mixed $subscriptionAutoCreation;

    /**
     * Prepend all queue names with this prefix.
     *
     * @var string
     */
    protected mixed $queuePrefix = '';

    /**
     * Create a new GCP PubSub instance.
     *
     * @param PubSubClient $pubSub
     * @param string $default
     * @param bool $topicAutoCreation
     * @param bool $subscriptionAutoCreation
     * @param string $queuePrefix
     */
    public function __construct(
        PubSubClient $pubSub,
        string       $default,
        bool         $topicAutoCreation = true,
        bool         $subscriptionAutoCreation = true,
        string       $queuePrefix = ''
    ) {
        $this->pubsub = $pubSub;
        $this->default = $default;
        $this->queuePrefix = $queuePrefix;
        $this->topicAutoCreation = $topicAutoCreation;
        $this->subscriptionAutoCreation = $subscriptionAutoCreation;
    }

    /**
     * Get the size of the queue.
     * PubSubClient have no method to retrieve the size of the queue.
     * To be updated if the API allow to get that data.
     *
     * @param  string  $queue
     *
     * @return int
     */
    public function size($queue = null): int
    {
        return 0;
    }

    /**
     * Push a new job onto the queue.
     *
     * @param  string|object  $job
     * @param  mixed   $data
     * @param  string  $queue
     *
     * @return string
     */
    public function push($job, $data = '', $queue = null): string
    {
        return $this->pushRaw($this->createPayload($job, $this->getQueue($queue), $data), $queue);
    }

    /**
     * Push a raw payload onto the queue.
     *
     * @param  string  $payload
     * @param  string  $queue
     * @param  array   $options
     *
     * @return string
     */
    public function pushRaw($payload, $queue = null, array $options = []): string
    {
        $topic = $this->getTopic($queue, $this->topicAutoCreation);

        $this->subscribeToTopic($topic);

        $publish = ['data' => base64_encode($payload)];

        if (! empty($options)) {
            $publish['attributes'] = $this->validateMessageAttributes($options);
        }

        $topic->publish($publish);
        $decoded_payload = json_decode($payload, true);

        return $decoded_payload['id'];
    }

    /**
     * Push a new job onto the queue after a delay.
     *
     * @param  \DateTimeInterface|\DateInterval|int  $delay
     * @param  string|object  $job
     * @param  mixed   $data
     * @param  string  $queue
     *
     * @return string
     */
    public function later($delay, $job, $data = '', $queue = null): string
    {
        return $this->pushRaw(
            $this->createPayload($job, $this->getQueue($queue), $data),
            $queue,
            ['available_at' => (string) $this->availableAt($delay)]
        );
    }

    /**
     * Pop the next job off of the queue.
     *
     * @param null $queue
     * @return Job|null
     */
    public function pop($queue = null): Job|null
    {
        $topic = $this->getTopic($this->getQueue($queue));

        if ($this->topicAutoCreation && ! $topic->exists()) {
            return null;
        }

        $subscription = $topic->subscription($this->getSubscriberName($queue));

        $messages = $subscription->pull([
            'returnImmediately' => true,
            'maxMessages' => 1,
        ]);

        if (empty($messages) || count($messages) < 1) {
            return null;
        }

        $available_at = $messages[0]->attribute('available_at');
        if ($available_at && $available_at > time()) {
            return null;
        }

        $this->acknowledge($messages[0], $queue);

        return new JobProcessor(
            $this->container,
            $this,
            $messages[0],
            $this->connectionName,
            $this->getQueue($queue)
        );
    }

    /**
     * Push an array of jobs onto the queue.
     *
     * @param  array   $jobs
     * @param  mixed   $data
     * @param  string  $queue
     *
     * @return array
     */
    public function bulk($jobs, $data = '', $queue = null): array
    {
        $payloads = [];

        foreach ((array) $jobs as $job) {
            $payload = $this->createPayload($job, $this->getQueue($queue), $data);
            $payloads[] = ['data' => base64_encode($payload)];
        }

        $topic = $this->getTopic($this->getQueue($queue), $this->topicAutoCreation);
        $this->subscribeToTopic($topic);

        return $topic->publishBatch($payloads);
    }

    /**
     * Acknowledge a message.
     *
     * @param Message $message
     * @param null $queue
     */
    public function acknowledge(Message $message, $queue = null)
    {
        $subscription = $this->getTopic($this->getQueue($queue))->subscription($this->getSubscriberName($queue));
        $subscription->acknowledge($message);
    }

    /**
     * Republish a message onto the queue.
     *
     * @param Message $message
     * @param null $queue
     * @param array $options
     * @param int $delay
     * @return array
     */
    public function republish(Message $message, $queue = null, array $options = [], int $delay = 0): array
    {
        $topic = $this->getTopic($this->getQueue($queue));

        $options = array_merge([
            'available_at' => (string) $this->availableAt($delay),
        ], $this->validateMessageAttributes($options));

        return $topic->publish([
            'data' => $message->data(),
            'attributes' => $options,
        ]);
    }

    /**
     * Create a payload array from the given job and data.
     *
     * @param  mixed  $job
     * @param  string  $queue
     * @param  mixed  $data
     * @return array
     */
    protected function createPayloadArray($job, $queue, $data = ''): array
    {
        return array_merge(parent::createPayloadArray($job, $this->getQueue($queue), $data), [
            'id' => $this->getRandomId(),
        ]);
    }

    /**
     * Check if the attributes array only contains key-values
     * pairs made of strings.
     *
     * @param array $attributes
     *
     * @return array
     *@throws \UnexpectedValueException
     */
    private function validateMessageAttributes(array $attributes): array
    {
        $attributes_values = array_filter($attributes, 'is_string');

        if (count($attributes_values) !== count($attributes)) {
            throw new \UnexpectedValueException('PubSubMessage attributes only accept key-value pairs and all values must be string.');
        }

        $attributes_keys = array_filter(array_keys($attributes), 'is_string');

        if (count($attributes_keys) !== count(array_keys($attributes))) {
            throw new \UnexpectedValueException('PubSubMessage attributes only accept key-value pairs and all keys must be string.');
        }

        return $attributes;
    }

    /**
     * Get the current topic.
     *
     * @param $queue
     * @param bool|string $create
     *
     * @return Topic
     */
    public function getTopic($queue, bool|string $create = false): Topic
    {
        $queue = $this->getQueue($queue);
        $topic = $this->pubsub->topic($queue);

        // don't check topic if automatic creation is not required, to avoid additional administrator operations calls
        if ($create && ! $topic->exists()) {
            $topic->create();
        }

        return $topic;
    }

    /**
     * Create a new subscription to a topic.
     *
     * @param Topic $topic
     *
     * @return Subscription
     */
    public function subscribeToTopic(Topic $topic): Subscription
    {
        $subscription = $topic->subscription($this->getSubscriberName($topic->name()));

        // don't check subscription if automatic creation is not required, to avoid additional administrator operations calls
        if ($this->subscriptionAutoCreation && ! $subscription->exists()) {
            $subscription = $topic->subscribe($this->getSubscriberName($topic->name()));
        }

        return $subscription;
    }

    /**
     * Get subscriber name.
     *
     * @return mixed
     */
    public function getSubscriberName($queue): string
    {
        $positionInString = strrpos($queue, '/', -1);

        if ($positionInString === false) {
            return $queue. '-sub';
        }

        return substr($queue, $positionInString+1). '-sub';
    }

    /**
     * Get the PubSub instance.
     *
     * @return PubSubClient
     */
    public function getPubSub(): PubSubClient
    {
        return $this->pubsub;
    }

    /**
     * Get the queue or return the default.
     *
     * @return string
     */
    public function getQueue($queue)
    {
        $queue = $queue ?: $this->default;

        if (! $this->queuePrefix || Str::startsWith($queue, $this->queuePrefix)) {
            return $queue;
        }

        return $this->queuePrefix.$queue;
    }

    /**
     * Get a random ID string.
     *
     * @return string
     */
    protected function getRandomId(): string
    {
        return Str::random(32);
    }
}
